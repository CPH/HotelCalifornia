package Hotel.Room;

/**
 * Created by becva on 29.09.2016.
 */
public class DeluxRoom extends Room {
    public DeluxRoom(float price, int bedCount) {
        super(price,bedCount);
        System.out.println("Delux created");
    }
}
