package Hotel.Room;

import Hotel.Minibar.MiniBar;

/**
 * Created by becva on 29.09.2016.
 */
abstract public class Room {
    protected int id;
    protected float price;
    protected int bedCount;
    protected boolean available = true;
    protected MiniBar miniBar;
    private static int count;

    public Room(float price, int bedCount) {
        this.price = price;
        this.bedCount = bedCount;
        count++;
        this.id = count;
    }

    public void setMiniBar(MiniBar miniBar) {
        this.miniBar = miniBar;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isAvailable(){
        return available;
    }

    public int getBedCount(){
        return bedCount;
    }

    public int getId(){
        return id;
    }
}
