package Hotel;
import java.util.*;
import Hotel.Room.Room;
import Libs.Form.Form;


/**
 *
 * @author mre44
 */
public class Booker {
    private Hotel hotel;
    Scanner input = new Scanner(System.in);
    private static Form bookingForm = new Form();
    static {
        bookingForm.addNumber("type","Choose type (1,2,3 = delux)")
            .setRange(1,3);
        bookingForm.addNumber("action","Press 1 to book, 2 to cancel a booking")
            .setRange(1,2);
        bookingForm.addNumber("room","Which room do you want to book or cancel?");
    }

    public Booker(Hotel hotel){
        this.hotel = hotel;
    }

    public static void book(Room room) {
        if (room.isAvailable()){
            room.setAvailable(false);
            System.out.println("You just booked room: "+room.getId());
        } else {
            System.out.println("Room "+room.getId()+" is not available");
        }
    }

    public static void cancel(Room room) {
        if (!room.isAvailable()){
            room.setAvailable(true);
            System.out.println("You just cancelled your booking of room: "+room.getId());
        } else {
            System.out.println("Room "+room.getId()+" was not canceled, because the room is not booked");
        }
    }

    public void bookingMenu(){
        bookingForm.askForField("type");
        hotel.availableRooms((int) bookingForm.getNumber("type").getValue());

        Room room = askForRoom();
        bookingForm.askForField("action");
        int action = (int) bookingForm.getNumber("action").getValue();
        switch(action){
            case 1:
                book(room);
                break;
            case 2:
                cancel(room);
                break;
        }
    }

    private Room askForRoom() {
        while (true) {
            bookingForm.askForField("room");
            int roomNumber = (int) bookingForm.getNumber("room").getValue();
            try {
                return hotel.getRoom(roomNumber);
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Room with index " + roomNumber + " doesn't exist.");
            }
        }
    }
}
