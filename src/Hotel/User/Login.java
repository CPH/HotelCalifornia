/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hotel.User;
import Libs.Form.Form;

import java.util.ArrayList;

/**
 *
 * @author mre44
 */
public class Login {
    
    ArrayList<User> members = new ArrayList<>();
    public User loggedUser;
    private static Form userForm = new Form();
    static {
        userForm.addText("name","Please enter your name");
        userForm.addText("password","Please enter your password");
    }

    public void addUser(String name, String password, int type) {
        User member = new User(name, password, type);
        members.add(member);
    }

    public void createUser(){
        userForm.askForAllFields();
        addUser(
            userForm.get("name").toString(),
            userForm.get("password").toString(),
            User.USER
        );
    }

    public boolean authenticate(){
        String name = userForm.askForField("name");
        String password = userForm.askForField("password");
        for(int i = 0; i < members.size(); i++){
            if (name.equals(members.get(i).name)){
                if (password.equals(members.get(i).password)){
                    loggedUser = members.get(i);
                    return true;
                }
            }
        }
        return false;
    }
    
}//login
    

