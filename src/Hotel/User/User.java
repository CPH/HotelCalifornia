/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hotel.User;

/**
 *
 * @author mre44
 */
public class User {
    public static final int USER = 0;
    public static final int ADMIN = 1;

    String name;
    String password;
    int type;

    public User (String name, String password, int type){
        this.name= name;
        this.password = password;
        this.type = type;
    }
}
