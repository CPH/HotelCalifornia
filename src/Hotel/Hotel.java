package Hotel;

import Hotel.Minibar.MiniBar;
import Hotel.Room.DeluxRoom;
import Hotel.Room.Room;
import Hotel.Room.NormalRoom;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by becva on 29.09.2016.
 */
public class Hotel {
    private static Random random = new Random();
    // TODO: make it private again
    private ArrayList<Room> rooms = new ArrayList<>();

    // TODO: create factory (Hotel should not care about creating new rooms)
    public void createNormalRoom(float price,int bedCount) {
        if(bedCount == 2 || bedCount == 1) {
            Room room = new NormalRoom(price,bedCount);
            createRoom(room);
        }
    }
    public void createDeluxRoom(float price,int bedCount) {
        Room room = new DeluxRoom(price,bedCount);
        createRoom(room);
    }
    private void createRoom(Room room) {
        room.setMiniBar(new MiniBar());
        // Temporary implementation of random booking status of room when created
        // So we don't have to implement the file reading since it is obiviously not clear to everybody
        // Thanks to this, we have some rooms booked some not booked (for testing purpouses) and its different every build
        if((random.nextInt(3)+1) == 1) {
            room.setAvailable(false);
        }
        rooms.add(room);
    }

    public Room getRoom(int index) {
        return rooms.get(index);
    }

    public void availableRooms(int key){
        int bookedRooms=0;
        for (int i=0; i < rooms.size(); i++){
            if(rooms.get(i).isAvailable()){
                switch(key){
                    case 1:
                    case 2:
                        if (rooms.get(i) instanceof NormalRoom){
                            if (rooms.get(i).getBedCount()== key){
                                System.out.println(key+"room number: "+(i));}}
                        break;
                    case 3:
                        if (rooms.get(i) instanceof DeluxRoom){
                            System.out.println("Delux room number: "+(i));}
                        break;
                    default: break;

                }//switch
            }//if
            else bookedRooms++;
        }//for
        if (bookedRooms==rooms.size())
            System.out.println("Sorry, all rooms are booked");
    }// availableRooms

    public String toString() {
        return rooms.toString();
    }
}
