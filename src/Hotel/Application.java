package Hotel;

import Hotel.User.Login;
import Hotel.User.User;
import Libs.Form.Form;

import java.util.Scanner;

/**
 * Created by becva on 29.09.2016.
 */
public class Application {
    private static Hotel hotel;
    private static Login logger = new Login();
    private static Scanner input= new Scanner(System.in);
    private static Form menuForm = new Form();
    static {
        menuForm.addText("masterMenu","Choose operation (booking,minibar,income)")
            .setAllowedValues(new String[]{"b","m","i","booking","minibar","income"});
        menuForm.addContinue("haveAccount","Do you have account?");
        menuForm.addContinue("addingUsers","Do you want to continue adding users?");
    }


    public static void main(String[] args) {
        hotel = new Hotel();
        Booker booker = new Booker(hotel);
        createRooms();

        logger.addUser("Admin","123", User.ADMIN);
        loginMenu();
        while (true){
            switch (menuForm.askForField("masterMenu").charAt(0)) {
                case 'b':
                    booker.bookingMenu();
                    break;
                case 'm':

                    break;
                case 'i':

                    break;
            }
        }
    }
    private static void loginMenu() {
        while (true){
            if(!haveAccount()) {
                logger.createUser();
            } else {
                if(logger.authenticate()) {
                    System.out.println("Correct login attempt");
                    break;
                } else {
                    System.out.println("Wrong login attempt");
                }
            }
        }
    }
    private static boolean haveAccount() {
        return menuForm.askForField("haveAccount").substring(0,1).equalsIgnoreCase("y");
    }
    // TODO: do creation of rooms in the future

    private static void createRooms() {
        for (int i = 0; i < 5; i++) {
            hotel.createNormalRoom((float) 100.0,1);
        }
        for (int i = 0; i < 5; i++) {
            hotel.createNormalRoom((float) 200.0,2);
        }
        for (int i = 0; i < 5; i++) {
            hotel.createDeluxRoom((float) 2000.0*(i+1),(i+1));
        }
    }

    private static void browseRoomsMenu() {
        System.out.println("Which type of room would you like to book? Type 1 for Single, 2 for Double and 3 for Delux");
        int answer= input.nextInt();
        hotel.availableRooms(answer);
    }




}//class
