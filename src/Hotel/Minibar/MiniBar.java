package Hotel.Minibar;

import java.util.ArrayList;

/**
 * Created by becva on 29.09.2016.
 */
public class MiniBar {
    
    ArrayList<Item> minibar= new ArrayList();
    float revenue=0;

    public MiniBar(){
        Item Coke = new Item ("Coke", 20, 4);
        minibar.add(Coke);
        minibar.add(new Item("Juice", 20, 4));
        minibar.add(new Item("Tonic", 20, 4));
        minibar.add(new Item("Whiskey", 50, 4));
        minibar.add(new Item("Gin", 50, 4));
        minibar.add(new Item("Vodka", 50, 4));
        minibar.add(new Item("Nuts", 10, 4));
        minibar.add(new Item("Chips", 10, 4));
        minibar.add(new Item("Chocolate", 5, 4));
        minibar.add(new Item("Candies", 5, 4));
    }


    public void browseMiniBar(){
        for(int i = 0 ; i< minibar.size(); i++)
            System.out.println("item: "+minibar.get(i).itemName+ ", price per unit: "+minibar.get(i).price+", quantity available: " +minibar.get(i).quantity);
    }

    public void consumeItem(String item, int quantity){
        for (int i=0; i<minibar.size(); i++) {
            if (minibar.get(i).itemName.equals(item)) {
                revenue += minibar.get(i).price * minibar.get(i).quantity;
                minibar.get(i).quantity -= quantity;
            }
        }
    }

    public void restock(String item, int quantity){
        for (int i=0; i<minibar.size(); i++) {
            if (minibar.get(i).itemName.equals(item)) {
                minibar.get(i).quantity += quantity;
            }
        }
    }

}
